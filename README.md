#ABA File Generator
Issued by the Australian Bankers Association (ABA) the ABA or Cemtext file format is a 
format used by banks to allow for batch transactions.

##File types
This package allows you to generate credit and debit ABA files, use constants to specify the file type:

```php
AbaGenerator::DIRECT_ENTRY_CREDIT // Generate credit file
AbaGenerator::DIRECT_ENTRY_DEBIT // Generate debit file
```

##Validation rules
During the file generation the data used to build the content is validated based on two rules which are 
`Alpha` and `Numeric`.

###Alpha
Properties that are marked `Alpha` (Alphanumeric) are limited to:
* Letters: A-Z, a-z
* Numbers: 0-9
* The following Characters: spaces ( ), ampersands (&), apostrophes ('), commas (,), hyphens (-), 
full stops (.), forwards slashes (/), plus sign (+), dollar sign ($), exclamation mark (!), 
percentage sign (%), left parenthesis ((), right parenthesis ()), asterisk (*), number sign (#),
equal sign (=), colon (:), question mark (?), left square bracket ([), right square bracket (]), 
underscore (_), circumflex (^), at symbol (@)

###Numeric
Properties that are marked `Numeric` are limited to:
* Numbers: 0-9

##Exceptions
Multiple exceptions are thrown by the package:
* `Payment\AbaFile\Exceptions\InvalidArgumentException`: Thrown when a function argument is invalid
* `Payment\AbaFile\Exceptions\ValidationErrorsException`: Thrown when a property does not satisfy its
associated validation rule

##ClearingAccount and DetailRecord
Files are generated based on `AbaClearingAccount` and `AbaDetailRecord` objects which you can instantiate as
following:

```php
use Payment\AbaFile\AbaClearingAccount;
use Payment\AbaFile\AbaDetailRecord;

//Fluent setters
$clearingAccount = new AbaClearingAccount();
$clearingAccount
    ->setCreditName('creditName')
    ->setCreditDescription('creditDescription')
    //...
;
$detailRecord = new AbaDetailRecord();
$detailRecord
    ->setAmount(45435)
    ->setBsb('083-163')
    //...
;

//Array of properties through constructor
$clearingAccount = new AbaClearingAccount([
    'creditName' => 'creditName',
    'creditDescription' => 'creditDescription',
    //...
]);
$detailRecord = new AbaDetailRecord([
    'amount' => 45435,
    'bsb' => '083-163'
]);
```

To set properties through the constructor just pass an associative array with the property name as key when
you instantiate it.

###ClearingAccount properties
* **creditId (Numeric):** User ID used in Descriptive Record for credit file
* **creditName (Alpha):** User name used in Descriptive Record for credit file
* **creditDescription (Alpha):** Description of payments used in Descriptive Record for credit file
* **debitId (Numeric):** User ID used in Descriptive Record for debit file
* **debitName (Alpha):** User name used in Descriptive Record for debit file
* **debitDescription (Alpha):** Description of payments used in Descriptive Record for debit file
* **accountName (Alpha):** Name of the bank account
* **accountNumber (Alpha):** Number of the bank account
* **bsb (Alpha):** BSB of the bank account

###DetailRecord properties
* **amount (Numeric):** Amount of the payment associated to the record
* **bsb (Alpha):** BSB of the bank account associated to the record
* **accountName (Alpha):** Name of the bank account associated to the record
* **accountNumber (Alpha):** Number of the bank account associated to the record
* **lodgementReference (Alpha):** Reference of the payment associated to the record

##Usage
```php
use Payment\AbaFile\AbaClearingAccount;
use Payment\AbaFile\AbaDetailRecord;
use Payment\AbaFile\AbaGenerator;

//Instantiate clearing account
$clearingAccount = new AbaClearingAccount([
    'creditName' => 'creditName',
    'creditDescription' => 'creditDescription',
    //...
]);

//Instantiate list of detail records
$detailRecord1 = new AbaDetailRecord([
    'amount' => 45435,
    'bsb' => '083-163'
]);
$detailRecord2 = new AbaDetailRecord([
    'amount' => 45435,
    'bsb' => '083-163'
]);
//...
$detailRecords = [
    $detailRecord1,
    $detailRecord2,
    //...
];

//Instantiate ABA file generator for credit file
$abaFile = new AbaGenerator(
    AbaGenerator::DIRECT_ENTRY_CREDIT, //File type
    $merchandId, //Associated merchant ID
    $clearingAccount,
    $detailRecords,
    /* $internalFileId = false */ //Option to add an internal ID to the file name
    /* $processDate = null */ //Option to set manually the date of processment as DateTime object
);

//Retrieve file name and content
$fileName = $abaFile->getFileName();
$fileContent = $abaFile->getFileContent();
```
