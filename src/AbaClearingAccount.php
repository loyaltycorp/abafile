<?php

namespace Payment\AbaFile;

class AbaClearingAccount extends AbaObject
{
    protected $attributes = [
        'creditId',
        'creditName',
        'creditDescription',
        'debitId',
        'debitName',
        'debitDescription',
        'accountName',
        'accountNumber',
        'bsb'
    ];
}
