<?php

namespace Payment\AbaFile;

class AbaDetailRecord extends AbaObject
{
    protected $attributes = [
        'amount',
        'bsb',
        'accountNumber',
        'accountName',
        'lodgementReference'
    ];
}
