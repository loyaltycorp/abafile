<?php

namespace Payment\AbaFile;

use Payment\AbaFile\Exceptions\InvalidArgumentException;
use Payment\AbaFile\Exceptions\ValidationErrorsException;

class AbaGenerator implements FileGenerator
{
    const DIRECT_ENTRY_CREDIT = 0;
    const DIRECT_ENTRY_DEBIT = 1;
    const VALIDATION_RULE_ALPHA = 'alpha';
    const VALIDATION_RULE_NUMERIC = 'numeric';

    private static $fileTypes = [
        self::DIRECT_ENTRY_CREDIT => 'credit',
        self::DIRECT_ENTRY_DEBIT => 'debit'
    ];

    private static $validationRules = [
        self::VALIDATION_RULE_ALPHA => '/[^A-Za-z0-9 &\',-\.\/\+\$\!%\(\)\*\#=:\?\[\]_\^@]/',
        self::VALIDATION_RULE_NUMERIC => '/[^0-9-]/'
    ];

    private static $transactionCodes = [
        'debit' => 13,
        'credit' => 50,
        'au_gov_security_interest' => 51,
        'family' => 52,
        'pay' => 53,
        'pension' => 54,
        'allotment' => 55,
        'dividend' => 56,
        'debenture' => 57
    ];

    /** @var string */
    private $content;

    /** @var string */
    private $fileName;

    /** @var \DateTime */
    private $processDate;

    /**
     * AbaGenerator constructor.
     *
     * @param int                $fileType
     * @param string             $merchantId
     * @param AbaClearingAccount $clearingAccount
     * @param array              $detailRecords
     * @param bool               $internalFileId
     * @param \DateTime          $processDate
     *
     * @throws InvalidArgumentException If file type is invalid
     */
    public function __construct(
        $fileType,
        $merchantId,
        AbaClearingAccount $clearingAccount,
        array $detailRecords,
        $internalFileId = false,
        \DateTime $processDate = null
    )
    {
        if (!array_key_exists($fileType, self::$fileTypes)) {
            throw new InvalidArgumentException(sprintf('File type %d does not exist', $fileType));
        }
        if (!$internalFileId) {
            $internalFileId = date('Ymd_His');
        }

        $this->processDate = $processDate ?? new \DateTime();

        $this->fileName = sprintf(
            '%s-%s%s.aba',
            self::$fileTypes[$fileType],
            $merchantId,
            $internalFileId
        );

        switch ($fileType) {
            case static::DIRECT_ENTRY_CREDIT:
                $this->createCreditFile($clearingAccount, $detailRecords);
                break;
            case static::DIRECT_ENTRY_DEBIT:
                $this->createDebitFile($clearingAccount, $detailRecords);
                break;
        }
    }

    private function validateFields(array $fields)
    {
        $errors = [];

        foreach ($fields as $field => $validate) {
            $value = (string) $validate[0];
            $validation = $validate[1];

            if (strlen($value) == 0 || preg_match(self::$validationRules[$validation], $value)) {
                $errors[] = [
                    'field' => $field,
                    'value' => $value,
                    'validation' => $validation
                ];
            }
        }

        if (!empty($errors)) {
            throw new ValidationErrorsException($errors, 'Validation Errors');
        }
    }

    private function createCreditFile(AbaClearingAccount $clearingAccount, array $detailRecords)
    {
        // Record Type 0 ‐ Descriptive Record
        $l[] = $this->createDescriptiveRecord(
            $clearingAccount->getCreditName(),
            $clearingAccount->getCreditId(),
            $clearingAccount->getCreditDescription()
        );

        $total = 0;

        foreach ($detailRecords as $detailRecord) {
            /* @var AbaDetailRecord $detailRecord */
            $l[] = $dr[] = $this->createDetailsRecords(
                'credit',
                $detailRecord->getAmount(),
                $detailRecord->getBsb(),
                $detailRecord->getAccountNumber(),
                $detailRecord->getAccountName(),
                $detailRecord->getLodgementReference(),
                $clearingAccount
            );
            $total += $detailRecord->getAmount();
        }

        $l[] = $dr[] = $this->createDetailsRecords(
            'debit',
            $total,
            $clearingAccount->getBsb(),
            $clearingAccount->getAccountNumber(),
            $clearingAccount->getAccountName(),
            'credit-' . $this->processDate->format('dmy'),
            $clearingAccount
        );

        // Record Type 7 – File Total Record
        $l[] = $this->createFileTotalRecord($total, count($dr));

        $this->content = join("\r\n", $l);
    }

    /**
     * Record Type 0 ‐ Descriptive Record
     * One only per User ID
     *
     * @param string $userName
     * @param int    $userNumber
     * @param string $description
     *
     * @return string
     */
    private function createDescriptiveRecord($userName, $userNumber, $description = 'Payments')
    {
        $this->validateFields([
            'descriptive_record.userName' => [$userName, self::VALIDATION_RULE_ALPHA],
            'descriptive_record.userNumber' => [$userNumber, self::VALIDATION_RULE_NUMERIC],
            'descriptive_record.description' => [$description, self::VALIDATION_RULE_ALPHA]
        ]);

        /**
         * Record Type 0
         * Must be '0'
         * (1) 1
         */
        $f[] = '0';

        /**
         * Blank
         * Must be blank filled
         * (17) 2-18
         */
        $f[] = str_pad('', 17);

        /**
         * Reel Sequence Number
         * Must be numeric commencing at 01. Right justified. Zero filled.
         * (2) 19-20
         */
        $f[] = sprintf('%02d', 1);

        /**
         * Name of User Financial Institution
         * Must be an approved financial institution abbreviation. (Refer to APCA publication “BSB Numbers in Australia”)
         * (3) 21-23
         */
        $f[] = 'NAB';

        /**
         * Blank
         * Must be blank filled
         * (7) 24-30
         */
        $f[] = str_pad('', 7);

        /**
         * Name of User supplying file
         * Left justified. Blank filled. BECS EBCDIC character set valid. Must not be all blanks. Should be User preferred name
         * (26) 31-56
         */
        $f[] = $this->truncateAndPad($userName, 26);

        /**
         * Number of User supplying file
         * Must be User Identification Number, which is assigned by APCA and User Financial Institutions. Must be numeric. Right justified. Zero filled.
         * (6) 57-62
         */
        $f[] = sprintf('%06d', $userNumber);

        /**
         * Description of entries on file (for example “Payroll”)
         * BECS EBCDIC character set valid. Should accurately describe contents of file. Left justified. Blank filled if needed.
         * (12) 63-74
         */
        $description = strtoupper(substr($description, 0, 12));
        $f[] = $this->truncateAndPad($description, 12);

        /**
         * Date to be processed (i.e., the date transactions are released to all Financial Institutions)
         * Must be numeric and in the format of DDMMYY. Must be a valid date. Zero filled.
         * (6) 75-80
         */
        $f[] = $this->processDate->format('dmy');

        /**
         * Blank
         * Must be blank filled
         * (40) 81-120
         */
        $f[] = str_pad('', 40);

        return join('', $f);
    }

    private function truncateAndPad($string, $pad_length, $pad_string = " ", $pad_type = STR_PAD_RIGHT)
    {
        return str_pad(substr($string, 0, $pad_length), $pad_length, $pad_string, $pad_type);
    }

    private function formatBsb($bsb)
    {
        //If the provided bsb is missing the hyphen add it
        if (strlen($bsb) == 6) {
            $bsb = substr($bsb, 0, 3)."-".substr($bsb, 3);
        }
        //If the provided bsb is too long truncate it
        if (strlen($bsb) > 7) {
            $bsb = substr($bsb, 0, 7);
        }

        return $bsb;
    }

    /**
     * Record Type 1 – Detail Record
     * One or more
     *
     * @param string $type
     * @param int    $amount
     * @param string $bsb
     * @param string $accountNumber
     * @param string $accountName
     * @param string $reference
     * @param AbaClearingAccount $remitter
     *
     * @return string
     */
    private function createDetailsRecords($type, $amount, $bsb, $accountNumber, $accountName, $reference, AbaClearingAccount $remitter)
    {
        $this->validateFields([
            'detail_record.amount' => [$amount, self::VALIDATION_RULE_NUMERIC],
            'detail_record.bsb' => [$bsb, self::VALIDATION_RULE_ALPHA],
            'detail_record.accountNumber' => [$accountNumber, self::VALIDATION_RULE_ALPHA],
            'detail_record.accountName' => [$accountName, self::VALIDATION_RULE_ALPHA],
            'detail_record.reference' => [$reference, self::VALIDATION_RULE_ALPHA],
            'detail_record.remitter_bsb' => [$remitter->getBsb(), self::VALIDATION_RULE_NUMERIC],
            'detail_record.remitter_account' => [$remitter->getAccountNumber(), self::VALIDATION_RULE_ALPHA],
            'detail_record.remitter_name' => [$remitter->getAccountName(), self::VALIDATION_RULE_ALPHA]
        ]);

        /**
         * Record Type 1
         * Must be '1'
         * (1) 1
         */
        $f[] = '1';

        /**
         * BSB Number in format xxx‐xxx
         * Must be numeric with a hyphen in character position 5. Character positions 2 to 4 must be a valid 2 or 3 digit Institution Identifier issued by APCA. (Refer to APCA publication “BSB Numbers in Australia”)
         * (7) 2-8
         */
        $f[] = $this->formatBsb($bsb);

        /**
         * Account number to be credited/debited
         * Numeric, alpha (26 letters of the alphabet), hyphens & blanks only are valid. Must not contain all blanks or all zeros.Leading zeros, which are part of an account number, must be shown. Edit out hyphens where account number exceeds nine characters. Right justified. Blank filled. (NAB account numbers are numeric only)
         * (9) 9-17
         */
        $f[] = $this->truncateAndPad($accountNumber, 9, ' ', STR_PAD_LEFT);

        /**
         * Indicator
         * Must be a space or the letter ‘N’,‘T’,’W’,’X’ or ‘Y’.
         * ‘N’ – for new or varied BSB number or name details.
         * ‘T’ – for a drawing under a Transaction Negotiation Authority.
         *
         * Withholding Tax Indicators:
         * ‘W’ ‐ dividend paid to a resident of a country where a double tax agreement is in force.
         * ‘X’ – dividend paid to a resident of any other country.
         * ‘Y’ – interest paid to all non‐residents.
         * Where applicable, the amount of withholding tax is to appear in character positions 113‐120.
         * (1) 18
         */
        $f[] = ' ';

        /**
         * Transaction Code
         * Must only be valid industry standard trancodes. Only numeric valid.
         *
         * (2) 19-20
         */
        $f[] = $this->getTransactionCode($type);

        /**
         * Amount
         * Only numeric valid. Must be greater than zero. Show in cents without punctuations. Right justified. Zero filled. Unsigned.
         * (10) 21-30
         */
        $f[] = sprintf('%010d', $amount);

        /**
         * Title of Account to be credited/debited
         * BECS EBCDIC character set valid. Must not contain all blanks. Left justified. Blank filled.
         * Desirable format:
         * ‐ Surname followed by blank
         * ‐ Given names with blank between each name
         * (32) 31-62
         */
        $account_name = strtoupper(substr($accountName, 0, 32));
        $f[] = $this->truncateAndPad($account_name, 32);

        /**
         * Lodgement Reference (Reference as submitted by the User, indicating details of the origin of the entry, eg. Payroll Number, Invoice, Unique Customer Identifier)
         * NOTE: (This field is used by Financial Institutions for statement narrative)
         * BECS EBCDIC character set valid. Left justified. Blank Filled
         * (18) 63-80
         */
        $f[] = $this->truncateAndPad($reference, 18);

        /**
         * BSB Number in format xxx‐xxx. #Trace: to enable retracing of the entry to its source if necessary
         * Must be numeric with a hyphen in character position 84. Character positions 81‐83 must be a valid 2 or 3 digit Institution Identifier issued by APCA.(Refer to APCA publication “BSB Numbers in Australia”)
         * (7) 81-87
         */
        $f[] = $this->formatBsb($remitter->getBsb());

        /**
         * Account Number. #Trace: to enable retracing of the entry to its source if necessary
         * Numeric, alpha (26 letters of the alphabet), hyphens & blanks only are valid. Must not contain all blanks or all zeros. Leading zeros, which are part of an account number, must be shown. Edit out hyphens where account number exceeds nine characters. Right justified. Blank filled. (NAB account numbers are numeric only)
         * (9) 88-96
         */
        $f[] = $this->truncateAndPad($remitter->getAccountNumber(), 9, ' ', STR_PAD_LEFT);

        /**
         * Name of Remitter  (Name of originator of the entry. This may vary from Name of User.)
         * BECS EBCDIC character set valid. Must not contain all blanks. Left justified. Blank filled. NOTE: This field is used by Financial Institutions for statement narrative. When a Detail Record is used for a Financial Institution drawing under a Transaction Negotiation Authority, this field must contain the name of the Lodgement Financial Institution
         * (16) 97-112
         */
        $f[] = $this->truncateAndPad($remitter->getAccountName(), 16);

        /**
         * Amount of withholding tax
         * Numeric only valid. Show in cents without punctuation. Right justified. Zero filled. Unsigned.
         * (8) 113-120
         */
        $f[] = sprintf('%08d', 0);

        return join('', $f);

        /**
         * #Trace: A Trace Record must be the User’s own account or an account which the User has authority to operate and/or use as a Trace Record.
         * (Note: In many cases the Trace Record is also used to satisfy the requirements of the Anti‐Money Laundering and Counter‐Terrorism Financing Act 2006 (Cth) concerning the inclusion of Tracing Information in electronic funds transfer instructions. Please refer to Part 5 of the Act for details.)
         */
    }

    /**
     * Get Transaction Code
     * Must only be valid industry standard trancodes. Only numeric valid.
     *
     * ‘13’ ‐ Debit Items
     * ‘50’ ‐ Credit Items
     * ‘51’ ‐ Australian Govt. security interest
     * ‘52’ ‐ basic family payments/additional family payment
     * ‘53’ ‐ pay
     * ‘54’ ‐ pension
     * ‘55’ ‐ allotment
     * ‘56’ ‐ dividend
     * ‘57’ ‐ debenture / note interest
     *
     * @param string $type
     *
     * @return int
     *
     * @throws InvalidArgumentException If type is invalid
     */
    private function getTransactionCode($type)
    {
        if (!array_key_exists($type, self::$transactionCodes)) {
            throw new InvalidArgumentException('Transaction type ' . $type . ' does not exist.');
        }

        return self::$transactionCodes[$type];
    }

    /**
     * Record Type 7 – File Total Record
     * One only per User ID
     *
     * @return string
     */
    private function createFileTotalRecord($amount, $records)
    {
        $this->validateFields([
            'batch_control_record.amount' => [$amount, self::VALIDATION_RULE_NUMERIC],
            'batch_control_record.records' => [$records, self::VALIDATION_RULE_NUMERIC]
        ]);

        /**
         * Record Type 7
         * Must be '7'
         * (1) 1
         */
        $f[] = '7';

        /**
         * BSB Number
         * Must be 999‐999
         * (7) 2-8
         */
        $f[] = '999-999';

        /**
         * Blank
         * Must be blank filled
         * (12) 9-20
         */
        $f[] = str_pad('', 12);

        /**
         * File (User) Net Total Amount
         * Numeric only valid. Show in cents without punctuation. Right justified. Zero filled. Unsigned. Should be all zeros in a self‐balanced file
         * (10) 21-30
         */
        $f[] = sprintf('%010d', 0);

        /**
         * File (User) Credit Total Amount
         * Numeric only valid. Show in cents without punctuation. Right justified. Zero filled. Unsigned. Should equal either debit total if self‐balanced, or net total in a non‐balanced file.
         * (10) 31-40
         */
        $f[] = sprintf('%010d', $amount);

        /**
         * File (User) Debit Total
         * Numeric only valid. Show in cents without punctuation. Right justified. Zero filled. Unsigned. Should equal either credit total if self‐balanced, or net total in a non‐balanced file
         * (10) 41-50
         */
        $f[] = sprintf('%010d', $amount);

        /**
         * Blank
         * Must be blank filled
         * (24) 51-74
         */
        $f[] = str_pad('', 24);

        /**
         * File (User) count of Record Type 1
         * Numeric only valid. Right justified. Zero filled
         * (6) 75-80
         */
        $f[] = sprintf('%06d', $records);

        /**
         * Blank
         * Must be blank filled
         * (40) 81-120
         */
        $f[] = str_pad('', 40);


        return join('', $f);
    }

    private function createDebitFile(AbaClearingAccount $clearingAccount, $detailRecords)
    {
        // Record Type 0 ‐ Descriptive Record
        $l[] = $this->createDescriptiveRecord(
            $clearingAccount->getDebitName(),
            $clearingAccount->getDebitId(),
            $clearingAccount->getDebitDescription()
        );

        $total = 0;

        foreach ($detailRecords as $detailRecord) {
            /* @var AbaDetailRecord $detailRecord */
            $l[] = $dr[] = $this->createDetailsRecords(
                'debit',
                $detailRecord->getAmount(),
                $detailRecord->getBsb(),
                $detailRecord->getAccountNumber(),
                $detailRecord->getAccountName(),
                $detailRecord->getLodgementReference(),
                $clearingAccount
            );
            $total += $detailRecord->getAmount();
        }

        $l[] = $dr[] = $this->createDetailsRecords(
            'credit',
            $total,
            $clearingAccount->getBsb(),
            $clearingAccount->getAccountNumber(),
            $clearingAccount->getAccountName(),
            'debit-' . $this->processDate->format('dmy'),
            $clearingAccount
        );

        // Record Type 7 – File Total Record
        $l[] = $this->createFileTotalRecord($total, count($dr));

        $this->content = join("\r\n", $l);
    }

    /**
     * Get generated file name.
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Get generated file content.
     *
     * @return string
     */
    public function getFileContent()
    {
        return $this->content;
    }
}
