<?php

namespace Payment\AbaFile;

use Payment\AbaFile\Exceptions\InvalidArgumentException;

abstract class AbaObject
{
    /** @var array */
    protected $attributes = [];

    /** @var array */
    private $values = [];

    /**
     * AbaObject constructor.
     *
     * @param array $properties
     */
    public function __construct(array $properties = [])
    {
        foreach ($properties as $property => $value) {
            $setter = sprintf('set%s', ucfirst($property));
            $this->$setter($value);
        }
    }

    public function __call($name, $arguments)
    {
        // Set available types
        $types = ['get', 'set'];

        // Break calling method into type (get, has, is, set) and attribute
        preg_match('/^(' . implode('|', $types) . ')([\da-z]+)/i', $name, $matches);
        $type = strtolower($matches[1] ?? '');
        $attribute = lcfirst($matches[2]) ?? '';

        // The attribute being accessed must exist, the type must be valid and if setting an attribute
        // the user has permission to update it, if one of these things aren't true throw exception
        if (is_null($type) ||
            !in_array($attribute, $this->attributes)) {
            throw new InvalidArgumentException('Undefined attribute: ' . $attribute);
        }

        // Run method and return
        return call_user_func_array([$this, $type], array_merge([$attribute], $arguments));
    }

    /**
     * Set tha value of an attribute.
     *
     * @param string $attribute
     * @param string|int $value
     *
     * @return $this
     */
    private function set($attribute, $value)
    {
        $this->values[$attribute] = $value;

        return $this;
    }

    /**
     * Get the value of an attribute.
     *
     * @param string $attribute
     *
     * @return mixed|null
     */
    private function get($attribute)
    {
        return $this->values[$attribute] ?? null;
    }
}
