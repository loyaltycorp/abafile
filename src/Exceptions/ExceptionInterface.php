<?php

namespace Payment\AbaFile\Exceptions;

/**
 * Marker interface for all exceptions thrown by the library.
 */
interface ExceptionInterface
{
}
