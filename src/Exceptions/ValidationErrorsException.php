<?php

namespace Payment\AbaFile\Exceptions;

use Throwable;

class ValidationErrorsException extends \Exception implements ExceptionInterface
{
    /** @var array */
    private $errors;

    public function __construct(array $errors, $message = "", $code = 0, Throwable $previous = null)
    {
        $this->errors = $errors;
        $message = sprintf('%s. %s', $message, $this->getErrorsToString());

        parent::__construct($message, $code, $previous);
    }

    /**
     * Get validation errors.
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Get validation errors as string representation.
     *
     * @return string
     */
    public function getErrorsToString()
    {
        $pattern = '[field => %s, value => %s, validation => %s]';
        $errorsToString = '';

        foreach ($this->errors as $error) {
            $errorsToString .= sprintf($pattern, $error['field'], $error['value'], $error['validation']);
        }

        return $errorsToString;
    }
}
