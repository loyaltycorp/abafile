<?php

namespace Payment\AbaFile;

interface FileGenerator
{
    /**
     * Get generated file name.
     *
     * @return string
     */
    public function getFileName();

    /**
     * Get generated file content.
     *
     * @return string
     */
    public function getFileContent();
}
