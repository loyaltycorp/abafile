<?php

namespace Payment\AbaFile\Tests;

use Payment\AbaFile\AbaClearingAccount;
use Payment\AbaFile\AbaDetailRecord;
use Payment\AbaFile\AbaGenerator;
use Payment\AbaFile\Exceptions\InvalidArgumentException;
use Payment\AbaFile\Exceptions\ValidationErrorsException;

class AbaGeneratorTest extends AbaTestCase
{
    public function testInvalidFileType()
    {
        $this->expectException(InvalidArgumentException::class);

        new AbaGenerator(3, '', new AbaClearingAccount(), []);
    }

    public function testValidationErrorsThrowing()
    {
        $this->expectException(ValidationErrorsException::class);

        new AbaGenerator(
            AbaGenerator::DIRECT_ENTRY_CREDIT,
            'merchantId',
            new AbaClearingAccount($this->incompleteClearingAccountProperties),
            []
        );
    }

    public function testValidFileName()
    {
        $clearingAccount = new AbaClearingAccount($this->completeClearingAccountProperties);
        $merchantId = 'merchantId';

        $creditAbaFile = new AbaGenerator(AbaGenerator::DIRECT_ENTRY_CREDIT, $merchantId, $clearingAccount, []);
        $debitAbaFile = new AbaGenerator(AbaGenerator::DIRECT_ENTRY_DEBIT, $merchantId, $clearingAccount, []);

        //Starts with good file type
        $this->assertStringStartsWith('credit-', $creditAbaFile->getFileName());
        $this->assertStringStartsWith('debit-', $debitAbaFile->getFileName());

        //Contains merchant id
        $this->assertTrue(strpos($creditAbaFile->getFileName(), $merchantId) !== false);
        $this->assertTrue(strpos($debitAbaFile->getFileName(), $merchantId) !== false);

        //Ends with good extension
        $this->assertStringEndsWith('.aba', $creditAbaFile->getFileName());
        $this->assertStringEndsWith('.aba', $debitAbaFile->getFileName());
    }

    public function testValidCreditFileContent()
    {
        $file = __DIR__ . '/files/credit-test.aba';
        $creditFile = new AbaGenerator(
            AbaGenerator::DIRECT_ENTRY_CREDIT,
            'merchantId',
            $this->getClearingAccountForFileContent(),
            $this->getDetailRecordsForFileContent(),
            false,
            \DateTime::createFromFormat('d-m-Y', '10-08-2017')
        );

        $this->assertStringEqualsFile($file, $creditFile->getFileContent());
    }

    public function testValidDebitFileContent()
    {
        $file = __DIR__ . '/files/debit-test.aba';
        $creditFile = new AbaGenerator(
            AbaGenerator::DIRECT_ENTRY_DEBIT,
            'merchantId',
            $this->getClearingAccountForFileContent(),
            $this->getDetailRecordsForFileContent(),
            false,
            \DateTime::createFromFormat('d-m-Y', '10-08-2017')
        );

        $this->assertStringEqualsFile($file, $creditFile->getFileContent());
    }

    /**
     * Get clearing account object for valid file content testing.
     *
     * @return AbaClearingAccount
     */
    private function getClearingAccountForFileContent()
    {
        return new AbaClearingAccount([
            'creditName' => 'LOYALTY CORP AUSTRALIA',
            'creditId' => '492627',
            'creditDescription' => 'PAYMENTS',
            'debitName' => 'LOYALTY CORP AUSTRALIA',
            'debitId' => '492627',
            'debitDescription' => 'PAYMENTS',
            'bsb' => '083-170',
            'accountName' => 'TEST PAY RENT REWARDS',
            'accountNumber' => '739827524'
        ]);
    }

    /**
     * Get detail records objects array for valid file content testing.
     *
     * @return array
     */
    private function getDetailRecordsForFileContent()
    {
        return [
            new AbaDetailRecord([
                'amount' => 43452,
                'bsb' => '083-163',
                'accountNumber' => '1234356',
                'accountName' => 'TRUST ME',
                'lodgementReference' => '0049e2d7dd1288d086'
            ])
        ];
    }
}
