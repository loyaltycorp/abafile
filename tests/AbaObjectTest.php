<?php

namespace Payment\AbaFile\Tests;

use Payment\AbaFile\AbaClearingAccount;
use Payment\AbaFile\AbaDetailRecord;
use Payment\AbaFile\Exceptions\InvalidArgumentException;

class AbaObjectTest extends AbaTestCase
{
    public function testDetailRecordFluentSetters()
    {
        $detailRecord = new AbaDetailRecord();

        $detailRecord
            ->setAmount($this->incompleteDetailRecordProperties['amount'])
            ->setBsb($this->incompleteDetailRecordProperties['bsb'])
            ->setAccountNumber($this->incompleteDetailRecordProperties['accountNumber'])
            ->setAccountName($this->incompleteDetailRecordProperties['accountName'])
        ;

        $this->assertDetailRecord($detailRecord);
    }

    public function testDetailRecordConstructorSettersSuccess()
    {
        $this->assertDetailRecord(new AbaDetailRecord($this->incompleteDetailRecordProperties));
    }

    public function testDetailRecordConstructorSettersFailure()
    {
        $this->expectException(InvalidArgumentException::class);

        $this->assertDetailRecord(new AbaDetailRecord(
            array_merge($this->incompleteDetailRecordProperties, ['failureProp' => 'failureValue'])
        ));
    }

    public function testClearingAccountFluentSetters()
    {
        $clearingAccount = new AbaClearingAccount();

        $clearingAccount
            ->setCreditId($this->incompleteClearingAccountProperties['creditId'])
            ->setCreditName($this->incompleteClearingAccountProperties['creditName'])
            ->setCreditDescription($this->incompleteClearingAccountProperties['creditDescription'])
            ->setDebitId($this->incompleteClearingAccountProperties['debitId'])
            ->setDebitName($this->incompleteClearingAccountProperties['debitName'])
            ->setDebitDescription($this->incompleteClearingAccountProperties['debitDescription'])
            ->setAccountName($this->incompleteClearingAccountProperties['accountName'])
            ->setAccountNumber($this->incompleteClearingAccountProperties['accountNumber'])
        ;

        $this->assertClearingAccount($clearingAccount);
    }

    public function testClearingAccountConstructorSettersSuccess()
    {
        $this->assertClearingAccount(new AbaClearingAccount($this->incompleteClearingAccountProperties));
    }

    public function testClearingAccountConstructorSettersFailure()
    {
        $this->expectException(InvalidArgumentException::class);

        $this->assertClearingAccount(new AbaClearingAccount(
            array_merge($this->incompleteClearingAccountProperties, ['failureProp' => 'failureValue'])
        ));
    }

    /**
     * Assert a detail record object.
     *
     * @param AbaDetailRecord $detailRecord
     */
    private function assertDetailRecord(AbaDetailRecord $detailRecord)
    {
        $this->assertEquals($this->incompleteDetailRecordProperties['amount'], $detailRecord->getAmount());
        $this->assertEquals($this->incompleteDetailRecordProperties['bsb'], $detailRecord->getBsb());
        $this->assertEquals($this->incompleteDetailRecordProperties['accountNumber'], $detailRecord->getAccountNumber());
        $this->assertEquals($this->incompleteDetailRecordProperties['accountName'], $detailRecord->getAccountName());
        $this->assertNull($detailRecord->getLodgementReference());
    }

    /**
     * Assert a clearing account object.
     *
     * @param AbaClearingAccount $clearingAccount
     */
    private function assertClearingAccount(AbaClearingAccount $clearingAccount)
    {
        $this->assertEquals($this->incompleteClearingAccountProperties['creditId'], $clearingAccount->getCreditId());
        $this->assertEquals($this->incompleteClearingAccountProperties['creditName'], $clearingAccount->getCreditName());
        $this->assertEquals($this->incompleteClearingAccountProperties['creditDescription'], $clearingAccount->getCreditDescription());
        $this->assertEquals($this->incompleteClearingAccountProperties['debitId'], $clearingAccount->getDebitId());
        $this->assertEquals($this->incompleteClearingAccountProperties['debitName'], $clearingAccount->getDebitName());
        $this->assertEquals($this->incompleteClearingAccountProperties['debitDescription'], $clearingAccount->getDebitDescription());
        $this->assertEquals($this->incompleteClearingAccountProperties['accountName'], $clearingAccount->getAccountName());
        $this->assertEquals($this->incompleteClearingAccountProperties['accountNumber'], $clearingAccount->getAccountNumber());
        $this->assertNull($clearingAccount->getBsb());
    }
}
