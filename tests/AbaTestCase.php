<?php

namespace Payment\AbaFile\Tests;

use PHPUnit\Framework\TestCase;

abstract class AbaTestCase extends TestCase
{
    protected $incompleteDetailRecordProperties = [
        'amount' => 10000,
        'bsb' => '083-163',
        'accountNumber' => '123456',
        'accountName' => 'Account Test'
    ];

    protected $completeDetailRecordProperties = [
        'amount' => 10000,
        'bsb' => '083-163',
        'accountNumber' => '123456',
        'accountName' => 'Account Test',
        'lodgementReference' => 'fakeLodgementReference'
    ];

    protected $incompleteClearingAccountProperties = [
        'creditId' => '123456',
        'creditName' => 'fakeCreditName',
        'creditDescription' => 'fakeCreditDescription',
        'debitId' => '123456',
        'debitName' => 'fakeDebitName',
        'debitDescription' => 'fakeDebitDescription',
        'accountName' => 'Clearing Account Test',
        'accountNumber' => '123456'
    ];

    protected $completeClearingAccountProperties = [
        'creditId' => '123456',
        'creditName' => 'fakeCreditName',
        'creditDescription' => 'fakeCreditDescription',
        'debitId' => '123456',
        'debitName' => 'fakeDebitName',
        'debitDescription' => 'fakeDebitDescription',
        'accountName' => 'Clearing Account Test',
        'accountNumber' => '123456',
        'bsb' => '083-163'
    ];
}
